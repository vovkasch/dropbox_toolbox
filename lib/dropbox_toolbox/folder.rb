class DropboxToolbox::Folder
  attr_reader :client, :folder
  private :client, :folder

  TMP_PATH = 'tmp'

  def initialize(client, folder)
    @client = client
    @folder = folder
    FileUtils.mkdir_p TMP_PATH
  end

  def each_file(&block)
    folder_contents.each do |item|
      if !item['is_dir']
        get_and_process_file(item['path']) { |file| yield item, file }
      end
    end
  end

private

  def folder_contents
    metadata = client.metadata folder
    metadata['contents']
  end

  def get_and_process_file(path, &block)
    if block_given?
      file = get_file path
      yield file
      File.delete file
    end
  end

  def get_file(remote_path)
    contents = client.get_file_and_metadata(remote_path).first
    local_path = "#{TMP_PATH}/#{File.basename(remote_path)}"
    open(local_path, 'wb') {|f| f.puts contents }
    open local_path
  end
end
