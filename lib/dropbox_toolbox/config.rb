class DropboxToolbox::Config
  autoload :YamlFile, 'dropbox_toolbox/config/yaml_file'

  class << self
    attr_accessor :key, :secret, :access_token
  end

  def self.load_from_yaml(path)
    file = YamlFile.new path
    @key = file.key
    @secret = file.secret
    @access_token = file.access_token
    true
  end
end
