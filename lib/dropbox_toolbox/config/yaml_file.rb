require 'forwardable'
require 'ostruct'
require 'yaml'

class DropboxToolbox::Config::YamlFile
  extend Forwardable

  attr_reader :data
  private :data

  delegate [:key, :secret, :access_token] => :data

  def initialize(path)
    @data = OpenStruct.new YAML.load_file path
  end
end
