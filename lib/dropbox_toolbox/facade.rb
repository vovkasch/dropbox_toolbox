class DropboxToolbox::Facade
  attr_reader :key, :secret, :access_token, :client
  private :key, :secret, :access_token, :client

  def initialize(key=DropboxToolbox::Config.key, secret=DropboxToolbox::Config.secret, access_token=DropboxToolbox::Config.access_token)
    @key = key
    @secret = secret
    @access_token = access_token
  end

  def login
    @client = DropboxClient.new access_token
  end

  def import_new_checks!
    handle_each_file_in_folder('/new') do |remote_file_metadata, local_file|
      check_image = CheckImage.new.tap { |check_image| check_image.image = local_file }.save
      remove remote_file_metadata
    end
  end

  private

    def handle_each_file_in_folder(folder)
      DropboxToolbox::Folder.new(client, folder).each_file { |item, file| yield item, file }
    end

    def remove(metadata)
      pp 'removing ' + metadata.inspect
      client.file_delete metadata['path']
    end

    def upload(file, options)
      destination = options[:to]
      name = File.basename file
      client.put_file("#{destination}/#{name}", file)
      file
    end
end
