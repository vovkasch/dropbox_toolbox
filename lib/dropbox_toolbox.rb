require 'dropbox_sdk'

class DropboxToolbox
  autoload :Config, 'dropbox_toolbox/config'
  autoload :Folder, 'dropbox_toolbox/folder'
  autoload :Facade, 'dropbox_toolbox/facade'
  autoload :Version, 'dropbox_toolbox/version'
  include Version

  class << self
    private :new
  end

  def self.client
    @client ||= begin
      # DropboxToolbox::Config.load_from_yaml './config/dropbox.yml'
      client = DropboxToolbox::Facade.new
      client.login
      client
    end
  end
end
